export interface StudentProfile {
  STUDENT_NUMBER: string;
  COUNTRY: string;
  NAME: string;
  INTAKE_CODE: string;
  PROGRAMME: string;
  MENTOR_PROGRAMME_LEADER: string;
  STUDENT_STATUS: string;
  INTAKE_STATUS: string;
  PHOTO_NO: any;
  PL_NAME: string;
  MENTOR_NAME: string;
  PROVIDER_CODE: string;
  BLOCK: boolean;
  MESSAGE: string;
};
