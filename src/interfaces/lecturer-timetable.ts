export interface LecturerTimetable {
  module: string;
  since: string;
  until: string;
  location: string;
  room: string;
}
