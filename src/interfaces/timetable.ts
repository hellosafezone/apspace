export interface Timetable {
  INTAKE: string;
  MODID: string;
  DAY: string;
  LOCATION: string;
  ROOM: string;
  LECTID: string;
  DATESTAMP: string;
  DATESTAMP_ISO: string;
  TIME_FROM: string;
  TIME_TO: string;

  STAFFNAME: string; /* StaffDirectory.FULLNAME */
};
