export interface StaffDirectory {
  RefNo: number;
  PHOTO: string;
  ID: string;
  FULLNAME: string
  TITLE: string;
  DEPARTMENT: string;
  DEPARTMENT2: string;
  DEPARTMENT3: string;
  EMAIL: string;
  DID: string;
  EXTENSION: string;
  CODE: string;
};
