export interface Attendance {
  NAME: string;
  STUDENT_NUMBER: string;
  COURSE_CODE: string;
  COURSE_DESCRIPTION: string;
  MODULE_ATTENDANCE: string;
  PERCENTAGE: number;
  TOTAL_CLASSES: number;
  END_DATE: string;
  EXAM_ELIGIBILITY: string;
}
