export interface AttendanceLegend {
  BA: string;
  BF: string;
  BD: string;
  Eligible: string;
  'N/A': string;
  '***': string;
}
